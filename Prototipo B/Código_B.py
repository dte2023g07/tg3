import hashlib
import time

# Define una clase Block para representar cada bloque en la cadena
class Block:
    # El constructor de la clase Block recibe el índice, la marca de tiempo, los datos y el hash del bloque anterior
    def __init__(self, index, timestamp, data, previous_hash):
        # El índice del bloque en la cadena
        self.index = index
        # La marca de tiempo de cuando se creó el bloque
        self.timestamp = timestamp
        # Los datos que se almacenan en el bloque
        self.data = data
        # El hash del bloque anterior en la cadena
        self.previous_hash = previous_hash
        # El hash del bloque actual, que se calcula automáticamente utilizando la función SHA-256
        self.hash = self.calculate_hash()

    # Calcula el hash del bloque actual utilizando la función SHA-256
    def calculate_hash(self):
        sha = hashlib.sha256()
        sha.update(str(self.index).encode('utf-8') +
                   str(self.timestamp).encode('utf-8') +
                   str(self.data).encode('utf-8') +
                   str(self.previous_hash).encode('utf-8'))
        return sha.hexdigest()

# Define una clase Blockchain para manejar la cadena de bloques
class Blockchain:
    # El constructor de la clase Blockchain crea la cadena de bloques y agrega el bloque génesis
    def __init__(self):
        # La lista que contiene todos los bloques en la cadena
        self.chain = [self.create_genesis_block()]

    # Crea el bloque génesis con un índice de 0, una marca de tiempo actual, "Genesis Block" como datos y un hash anterior de 0
    def create_genesis_block(self):
        return Block(0, time.time(), "Genesis Block", "0")

    # Agrega un bloque a la cadena
    def add_block(self, new_block):
        # Asigna el hash del bloque anterior al hash del bloque actual
        new_block.previous_hash = self.chain[-1].hash
        # Calcula el hash del bloque actual
        new_block.hash = new_block.calculate_hash()
        # Agrega el bloque a la cadena
        self.chain.append(new_block)

    # Obtiene el bloque más reciente en la cadena
    def get_latest_block(self):
        return self.chain[-1]

# Define una clase Validator para validar nuevos bloques que se agregan a la cadena
class Validator:
    # El constructor de la clase Validator recibe la cadena de bloques que se validará
    def __init__(self, blockchain):
        self.blockchain = blockchain

    # Valida un bloque que se agrega a la cadena
    def validate(self, block):
        # Obtiene el bloque más reciente en la cadena
        latest_block = self.blockchain.get_latest_block()

        # Verifica que el índice del nuevo bloque sea el índice más reciente en la cadena + 1
        if latest_block.index + 1 != block.index:
            return False

        # Verifica que el hash anterior del nuevo bloque sea igual al hash del bloque más reciente en la cadena
        if latest_block.hash != block.previous_hash:
            return False

        # Verifica que el hash del nuevo bloque sea válido
        if block.hash != block.calculate_hash():
            return False

        # Si todas las validaciones son correctas, devuelve True
        return True

# Función de ejemplo
def blockchain_example():
    # Crea una nueva instancia de la cadena de bloques
    blockchain = Blockchain()

    # Crea algunos bloques y los agrega a la cadena
    block1 = Block(1, time.time(), "Datos del bloque 1", blockchain.chain[-1].hash)
    blockchain.add_block(block1)

    block2 = Block(2, time.time(), "Datos del bloque 2", blockchain.chain[-1].hash)
    blockchain.add_block(block2)

    block3 = Block(3, time.time(), "Datos del bloque 3", blockchain.chain[-1].hash)
    blockchain.add_block(block3)

    # Imprime la cadena de bloques completa
    print("Cadena de bloques completa:")
    for block in blockchain.chain:
        print("Index:", block.index)
        print("Timestamp:", block.timestamp)
        print("Data:", block.data)
        print("Hash:", block.hash)
        print("Previous Hash:", block.previous_hash)
        print("")

    # Crea una instancia del validador y verifica la cadena de bloques
    validator = Validator(blockchain)
    for block in blockchain.chain:
        print("El bloque", block.index, "es válido:", validator.validate(block))

print(blockchain_example())
