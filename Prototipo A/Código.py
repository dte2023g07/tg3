# Importar la librería hashlib para utilizar la función sha256 y la librería time para medir el tiempo
from hashlib import sha256
from time import time

# Número máximo de nonce que se va a probar para cada bloque
max_intentos = 10000000000

# Texto usado para generar el hash inicial
texto = 'DTE2023'

# Función que toma un texto como entrada, lo convierte a un hash sha256 y devuelve el resultado como un string
def SHA256(texto):
    return sha256(texto.encode('ascii')).hexdigest()

# Función de minado
def mine(numero_bloque, transaccion, hash_anterior, prefijo_de_ceros):

    # Cantidad de ceros requeridos para que el hash sea válido
    str_prefijo = '0' * prefijo_de_ceros

    for nonce in range(max_intentos):

        # Junto los valores de entrada en un string
        texto = str(numero_bloque) + transaccion + hash_anterior + str(nonce)
        # Generar un nuevo hash a partir del stringg
        hash_nuevo = SHA256(texto)

        # Si el hash comienza con los ceros requeridos, se ha encontrado un hash válido
        if hash_nuevo.startswith(str_prefijo):
            print('BTC minado con éxito en el nonce ' + str(nonce))
            return hash_nuevo

    # Si no se encuentra un hash válido, se levanta una excepción
    raise BaseException('No se encontró el hash')

# Función de ejemplo de minado
def main():
    # Crear una transacción
    transaccion = '''Manolo--->Pepe(20)'''
    # Establecer la dificultad (cantidad de ceros requeridos)
    dificultad = 3
    # Medir el tiempo antes de empezar a minar
    start = time()
    # Llamar a la función mine para encontrar un hash válido para el bloque número 2
    hash_nuevo =mine(2,transaccion,str(sha256(texto.encode('ascii')).hexdigest()),dificultad)

    # Imprimir el hash encontrado y el tiempo que tomó encontrarlo
    print(hash_nuevo)
    print(str(time()-start) + ' segundos')

# Llamar a la función principal
main()
